                     name "uppercase"

org 100h


jmp start


string db 20, 22 dup('?')

new_line db 0Dh,0Ah, '$'  ; kode baris baru.

start:            



lea dx, string

mov ah, 0ah
int 21h

mov bx, dx
mov ah, 0
mov al, ds:[bx+1]
add bx, ax ; arahkan ke ujung string.

mov byte ptr [bx+2], '$' ; letakkan dolar di akhir.

; int 21h / ah = 09h - output dari string di ds: dx.
; string harus diakhiri dengan tanda '$'.      

lea dx, new_line
mov ah, 09h
int 21h


lea bx, string

mov ch, 0
mov cl, [bx+1] ; mendapatkan size string.

jcxz null ; apakah string kosong?

add bx, 2 ; lewati karakter kontrol.

upper_case:
 
; periksa apakah itu huruf kecil:

cmp byte ptr [bx], 'a'
jb ok
cmp byte ptr [bx], 'z'
ja ok  

; ubah menjadi huruf besar:

; huruf besar tidak ada
; set bit ketiga, misalnya:
; 'a': 01100001b
; 'a': 01000001b
; topeng huruf besar: 11011111b

; hapus bit ketiga:
and byte ptr [bx], 11011111b

ok:
inc bx ; next char.
loop upper_case


; int 21h / ah = 09h - output dari string di ds: dx.
; string harus diakhiri dengan tanda '$'.
lea dx, string+2
mov ah, 09h
int 21h
 
; tekan sembarang tombol ....
mov ah, 0
int 16h 
 
 
null:
ret  ; return ke sistem operasi.
 
